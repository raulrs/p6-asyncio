import asyncio

async def hello():
    print("Hola")
    await asyncio.sleep(1)
    print("Hola otra vez")

async def main():
    await hello()

# Crear un nuevo bucle de eventos
loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)

try:
    # Ejecutar el bucle hasta que termine la función main
    loop.run_until_complete(main())
finally:
    # Cerrar el bucle de eventos
    loop.close()
