import socket
import json

UDP_IP = '127.0.0.1'
UDP_PORT = 12345

# Configuración del socket UDP para recibir datos JSON
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

try:
    # Espera a recibir el documento JSON
    data, addr = sock.recvfrom(1024)
    json_data = json.loads(data.decode())
    print(f"Documento JSON recibido de {addr}")

    # Verifica el tipo del documento
    if json_data['type'] == 'offer':
        sdp_offer = json.loads(json_data['sdp'])
        print("Oferta SDP recibida:")
        print(sdp_offer)

        # Modifica los puertos en la respuesta SDP
        for media in sdp_offer['media']:
            media['port'] = 34543

        # Construye la respuesta SDP JSON
        json_response = {
            'type': 'answer',
            'sdp': json.dumps(sdp_offer)
        }

        # Envía la respuesta SDP JSON al origen
        sock.sendto(json.dumps(json_response).encode(), addr)
        print(f"Respuesta SDP JSON enviada a {addr}")

finally:
    sock.close()
