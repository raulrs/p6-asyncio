import socket
import json
from sdp_transform import parse, write

sdp_offer = {
    'version': 0,
    'origin': {
        'username': 'user',
        'sessionId': 434344,
        'sessionVersion': 0,
        'netType': 'IN',
        'ipVer': 4,
        'address': '127.0.0.1'
    },
    'name': 'Session',
    'timing': {'start': 0, 'stop': 0},
    'connection': {'version': 4, 'ip': '127.0.0.1'},
    'media': [
        {
            'rtp': [
                {'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                {'payload': 96, 'codec': 'opus', 'rate': 48000}
            ],
            'type': 'audio',
            'port': 54400,
            'protocol': 'RTP/SAVPF',
            'payloads': '0 96',
            'ptime': 20,
            'direction': 'sendrecv'
        },
        {
            'rtp': [
                {'codec': 'H264', 'payload': 97, 'rate': 90000},
                {'codec': 'VP8', 'payload': 98, 'rate': 90000}
            ],
            'type': 'video',
            'port': 55400,
            'protocol': 'RTP/SAVPF',
            'payloads': '97 98',
            'direction': 'sendrecv'
        }
    ]
}

UDP_IP = '127.0.0.1'
UDP_PORT = 12345

# Convierte la oferta SDP a JSON
sdp_offer_json = json.dumps(sdp_offer)

# Configuración del socket UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    # Envía la oferta SDP al destino especificado
    sock.sendto(sdp_offer_json.encode(), (UDP_IP, UDP_PORT))
    print(f"Oferta SDP enviada a {UDP_IP}:{UDP_PORT}")
finally:
    sock.close()

