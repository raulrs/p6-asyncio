import socket
import json

UDP_IP = '127.0.0.1'
UDP_PORT = 12345

# Configuración del socket UDP para recibir datos SDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

try:
    # Espera a recibir la oferta SDP
    data, addr = sock.recvfrom(1024)
    sdp_offer_json = data.decode()
    print(f"Oferta SDP recibida de {addr}")

    # Convierte la oferta SDP de JSON a diccionario
    sdp_offer = json.loads(sdp_offer_json)

    # Crea una respuesta SDP independiente basada en la oferta recibida
    sdp_answer = {
        'version': sdp_offer['version'],
        'origin': sdp_offer['origin'],
        'name': sdp_offer['name'],
        'timing': sdp_offer['timing'],
        'connection': sdp_offer['connection'],
        'media': [
            {
                'rtp': [
                    {'payload': 0, 'codec': 'PCMU', 'rate': 8000},
                    {'payload': 96, 'codec': 'opus', 'rate': 48000}
                ],
                'type': 'audio',
                'port': 34543,  # Cambiado el puerto
                'protocol': 'RTP/SAVPF',
                'payloads': '0 96',
                'ptime': 20,
                'direction': 'sendrecv'
            },
            {
                'rtp': [
                    {'codec': 'H264', 'payload': 97, 'rate': 90000},
                    {'codec': 'VP8', 'payload': 98, 'rate': 90000}
                ],
                'type': 'video',
                'port': 34543,  # Cambiado el puerto
                'protocol': 'RTP/SAVPF',
                'payloads': '97 98',
                'direction': 'sendrecv'
            }
        ]
    }

    # Convierte la respuesta SDP a JSON
    sdp_answer_json = json.dumps(sdp_answer)

    # Envía la respuesta SDP al origen
    sock.sendto(sdp_answer_json.encode(), addr)
    print(f"Respuesta SDP enviada a {addr}")

finally:
    sock.close()
